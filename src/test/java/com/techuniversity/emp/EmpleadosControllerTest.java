package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.EstadosPedido;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena(){
        String esperado = "I.S.A.A.C R.I.V.E.R.O";
        assertEquals(esperado, empleadosController.getCadena("isaac RivEro",  "."));

    }
    @Test
    public void testCadena2(){
        try {
            Utilidades.getCadena("isaac RivEro",  "..");
            fail("se espaba un separador diferente");
        } catch (BadSeparator bs) {  }
    }

    @ParameterizedTest
    @ValueSource(ints = {1,3,5,-3, 15, Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(ints = {2,4,10,-6, 30, Integer.MIN_VALUE})
    public void testEsPares(int numero){
        assertFalse(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    // informa a null todos los string u/o objetos
    @NullSource
    @EmptySource
    public void testEstaBlancoNull(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }
    @ParameterizedTest
    // informa a null y vacios todos los string u/o objetos
    @NullAndEmptySource
    // los caracteres de escape los elimina el .trim por l oque funciona
    @ValueSource(strings = {"", " ", "\t", "\n"})
    public void testEstaBlancoNull1(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testvalorarEstadoPedido(EstadosPedido estadosPedido){
       assertTrue(Utilidades.valorarEstadoPedido(estadosPedido));
    }
}


