package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
class RestEmpApplicationTests {

	@Test
	void contextLoads() {

	}
	@Autowired
	EmpleadosController empleadosController = new EmpleadosController();

	@Test
	public void homeTest() {
		String result =  empleadosController.home();
		assertEquals( "Don't worry, be happy", result);
	}

}