package com.techuniversity.emp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
class RestEmpApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestEmpApplication.class, args);
	}

}