package com.techuniversity.emp.utils;

public class BadSeparator extends Exception{

    @Override
    public String getMessage() {
        return "Error el separador es erroneo. No es correcto.";
    }
}
