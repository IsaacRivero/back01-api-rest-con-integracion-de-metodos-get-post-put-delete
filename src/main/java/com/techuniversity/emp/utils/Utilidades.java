package com.techuniversity.emp.utils;

import java.util.Locale;

public class Utilidades {

    public static String getCadena(String texto, String separador) throws BadSeparator{
        if ((separador.length() > 1) || (separador.trim().equals(""))) throw  new BadSeparator();
        char[] letras = texto.toUpperCase(Locale.ROOT).toCharArray();
        String resultado = "";
        for (char letra:letras) {
            if (letra != ' ' ) {
                resultado += letra + separador;
            } else {
                if (!resultado.trim().isEmpty()) {
                    resultado = resultado.substring(0,resultado.length() - 1);
                }
                resultado += letra;
            }
        }
        if (resultado.endsWith(separador)){
            resultado = resultado.substring(0,resultado.length() - 1);
        }
        return resultado;
    }

    public static  boolean esImpar (int numero) {
        System.out.println(numero % 2);
        return (numero % 2 != 0);
    }

    public static boolean estaBlanco(String texto) {
        return ((texto == null) || texto.trim().isEmpty());
    }

    public static boolean valorarEstadoPedido(EstadosPedido estadosPedido) {
        int valor = estadosPedido.ordinal();
        return ((valor >=0 ) && (valor <= 5));
    }
}
